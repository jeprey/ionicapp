angular.module('starter.controllers', [])
.controller('DashCtrl', function($scope, $window, Subjects) {})

.controller('SubjectsCtrl', function($scope, $window, Subjects, $state) {
	$scope.list = Subjects.all();
	
	$scope.remove = function(subjectName) {
		Subjects.remove(subjectName);
		alert('Successfully Deleted.');
	};
	
	$scope.go = function(subjectName) {
		$state.go('tab.subjects-students', { subjectName: subjectName });
	};
})

.controller('SubjectsNewCtrl', function($scope, Subjects, $state) {
	$scope.new = {};
	$scope.new.init = function(){
		$scope.new.subjectName = '';
	}
	
	$scope.new.submit = function() {
		Subjects.add($scope.new.subjectName);
		$state.go('tab.subjects');
		alert('Successfully Saved.');
	};
	
	$scope.new.init();
})

.controller('StudentsCtrl', function($rootScope, $scope, $stateParams, Students) {
	$scope.subject = $stateParams.subjectName;
	// console.log($scope.subject);
	$scope.students = Students.getAllStudentsBySubject($stateParams.subjectName);
	
	$scope.remove = function(studentName) {
		Students.remove(studentName);
		$scope.$emit('students:listChanged');
		alert('Successfully Deleted.');
	};
	
	$rootScope.$on('students:listChanged', function() {
		$scope.updateList();
	});
	
	$scope.updateList = function() {
		$scope.students  = Students.getAllStudentsBySubject($stateParams.subjectName);
	};
})

.controller('StudentsNewCtrl', function($scope, Students, $state, $ionicHistory, $stateParams) {
console.log($stateParams.subjectName); 
	$scope.new = {};
	$scope.new.init = function(){
		$scope.new.studentName = '';
		$scope.new.subject = $stateParams.subjectName;
	}
	
	$scope.new.submit = function() {
		Students.add($scope.new.studentName, $scope.new.subject);
		$scope.$emit('students:listChanged');
		$ionicHistory.goBack();
		alert('Successfully Saved.');
	};
	
	$scope.new.init();
})

.controller('CashFlowNewCtrl', function($scope, CashFlow, FlowType, $state) {
	$scope.new = {};
	$scope.new.init = function(){
		$scope.new.name = '';
		$scope.new.amount = '';
		$scope.new.category = '';
	}
	
	$scope.new.submit = function() {
		CashFlow.add($scope.new.category, $scope.new.amount,$scope.new.name);
		$state.go('tab.cashflow');
		alert('Successfully Saved.');
	};
	
	$scope.categories =  FlowType.all();
	$scope.new.init(); 
	
})

.controller('CashFlowCtrl',function($scope, CashFlow, FlowType){
  
	$scope.list = CashFlow.all();

	$scope.isIncome = function(item){
		var type = FlowType.getById(item.type);
		if(type==null){
			return false;
		}else if(type.isIncome){
			return true;
		}else{
			return false;
		}
	};
});