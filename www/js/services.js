angular.module('starter.services', [])

.factory('Subjects', function() {
	// localStorage.removeItem('subjects');
	var subjects = [];
	subjects = JSON.parse(localStorage.getItem('subjects')) || [];
	
	return {
		all:function(){
			return subjects
		},
		add:function(subjectName){
			subjects.push({
				subjectName:subjectName,
				created_at:new Date()
			});
		  
			this.save();
		},
		remove:function(subjectName) {
			subjectLength = subjects.length;
			for(var i=subjectLength-1; i>=0; i--) {
				if(subjects[i].subjectName == subjectName) {
					subjects.splice(i, 1);
				}
			}
			this.save();
		},
		save:function() {
			localStorage.removeItem("subjects");   
			localStorage.setItem('subjects', JSON.stringify(subjects));
		}
	};
	
})

.factory('Students', function() {
// localStorage.removeItem('students');
	var students = [];
	students = JSON.parse(localStorage.getItem('students')) || [];
	
	return {
		all:function(){
			return students
		},
		getAllStudentsBySubject:function(subjectName){
			var studentsInSubject = [];
			studentsLength = students.length;
			for(var i=0; i<studentsLength; i++) {
				if(students[i].subject == subjectName) {
					studentsInSubject.push(students[i]);
				}
			}
			
			return studentsInSubject
		},
		add:function(studentName, subject){
			students.push({
				studentName:studentName,
				subject:subject,
				created_at:new Date()
			});
		  
			this.save();
		},
		remove:function(studentName) {
			studentsLength = students.length;
			for(var i=studentsLength-1; i>=0; i--) {
				if(students[i].studentName == studentName) {
					students.splice(i, 1);
				}
			}
			this.save();
		},
		save:function() {
			localStorage.removeItem("students");   
			localStorage.setItem('students', JSON.stringify(students));
		}
	};
	
})

.factory('CashFlow',function(){

	var cashFlow = [];
  
	cashFlow = JSON.parse(localStorage.getItem('cashflow')) || [];
  
	return {
		all:function(){
			return cashFlow
		},

		add:function(type,amount,description){
			cashFlow.push({
				type:type,
				amount:amount,
				description:description,
				created_at:new Date()
			});
		  
			this.save();
		},
	
		save:function() {
			localStorage.setItem('cashflow', JSON.stringify(cashFlow));
		}
	};
})

.factory('FlowType',function(){ 
	var types = {
		'salary':{
			name:'Salary',
			description:'',
			isIncome:true
		},
		'food':{
			name:'Food',
			description:'',
			isIncome:false
		}
	};
  
	return {
		getById:function(id){
			if ( types.hasOwnProperty(id) ){
				return types[id];
			}else{
				return null;
			}
		},
	
		all:function(){
			return types
		}
	};
});
 